import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import './App.css';

import { getGenres } from './actions/genresActions';

class App extends Component {
  static propTypes = {
    //getMovies: PropTypes.func.isRequired,
    getGenres: PropTypes.func.isRequired,
    //genres: PropTypes.object.isRequired,
    //movies: PropTypes.object.isRequired,
    //filters: PropTypes.object.isRequired
  };
  componentDidMount() {
    //this.props.getMovies();
    this.props.getGenres();
  }
  render() {
    console.log(this.props);
    return (
      <div className="App">
        <ul>
          {
            // names.map(name => {
            //   return (
            //     <li key={name}>
            //       {name}
            //     </li>
            //   );
            // })
          }
        </ul>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  //genres: state.genres,
  //movies: state.movies,
  //filters: state.filters
});


const mapDispatchToProps = dispatch => ({
  //getMovies: () => dispatch(getMovies()),
  getGenres: () => dispatch(getGenres())
});

export default connect(mapStateToProps, mapDispatchToProps)(App);

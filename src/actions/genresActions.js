import * as api from '../services/api';
export const GET_GENRES_LOAD = 'GET_GENRES_LOAD';
export const GET_GENRES_ERROR = 'GET_GENRES_ERROR';
export const GET_GENRES_SUCCESS = 'GET_GENRES_SUCCESS';

export const getGenres = () => dispatch => {
  dispatch({
    type: [GET_GENRES_LOAD, GET_GENRES_SUCCESS, GET_GENRES_ERROR],
    payload: () => api.setGenres
  })
}

console.log(api.setGenres)
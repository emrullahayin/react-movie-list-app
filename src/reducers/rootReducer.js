import { combineReducers } from 'redux';
import genresReducer from './genresReducer';

export default combineReducers({
    genresReducer
});